---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4751-instruction
version-history-start-date: '2023-02-17T20:05:13Z'
---
# Calendar Fundamentals
**Estimated time needed:** 15 minutes

For this lab, you will use Microsoft\'s Outlook app at office.com, which is one of the most used online email and calendar applications worldwide. Any skills learned from Outlook.com can be applied to other calendar applications such as the desktop version of Microsoft Outlook, and Google Calendar.

## Objectives
After completing this lab, you will be able to:
- Create a meeting
- Attend a meeting
- Share a calendar


## Exercise 1 : Creating a Meeting
In this exercise, you will create a meeting.

1. To create a meeting, navigate to the calendar page by clicking on the **Calendar** button on the left side of the screen. The calendar page is where calendars can be viewed.

![](/images/Screenshot%20(225).png)

2. After clicking on the Calendar button, you will be directed to the main calendar page.

![](/images/Screenshot%20(307).png)

3. Click **New event**.

![](/images/Screenshot%20(226).png)

4. In the dialog box that appears, you fill in the meeting details, such as the *title*, *time*, *date*, and *description*.

![](/images/Screenshot%20(370).png)

5. In the **Title** box type *Sales Meeting*.

![](/images/Screenshot%20(371).png)

6. In the **Invite attendees** textbox, enter your email address.

![](/images/Screenshot%20(372)%20updated.png)

7. To make the meeting a Teams meeting, click the **Teams meeting** toggle switch.

![](/images/Screenshot%20(373)%20updated.jpg)

8.  Click **Send**.

![](/images/Screenshot%20(374)%20updated.png)

## Exercise 2 : Attending a Meeting
In this exercise, you will attend a meeting via the calendar event.

1. To attend a meeting, click on the meeting event in your calendar.

![](/images/Screenshot%20(375)%20updated.jpg)

2. Click **Join**.

![](/images/Screenshot%20(376)%20updated.jpg)

## Exercise 3 : Sharing a Calendar
In this exercise, you will share your calendar.

1. To share a calendar, navigate to the Calendar page by clicking the **Calendar** button on the left side of the screen.

![](/images/Screenshot%20(225).png)

2. On the Calendar page, in the toolbar, click **Share**.

![](/images/Screenshot%20(377)%20updated.png)

3. In the **Sharing and permissions** dialog box, enter the email address of a friend or colleague in the textbox and then click the drop-down arrow to display the list of sharing permissions.

![](/images/Screenshot%20(378)new.png)

4. Choose **Can view all details**, and then click **Share**.

![](/images/Screenshot%20(379)new.png)


## Congratulations! You have completed this lab and are ready for the next topic.


## Author(s)
Andrew Pfeiffer

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 |0.6   |Steve Ryan   | Removed \'About Lab Sessions\' paragraph |
| 2023-03-09 |0.5   |Steve Ryan   | ID review |
| 2023-02-23 |0.4   |Andrew Pfieffer   | Final edits and screenshot updates |
| 2023-02-22 |0.3   |Steve Ryan   | Minor edit to opening paragraph |
| 2023-02-20 |0.2   |Steve Ryan   | Minor edits / screenshot changes  |
| 2023-02-20 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
