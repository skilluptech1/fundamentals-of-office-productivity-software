---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4949-instruction
version-history-start-date: '2023-02-28T21:34:05Z'
---
# Hands-on Lab: Working with Files & Folders

**Estimated time needed:** 20 minutes

For this lab, you will be using the Windows 11 operating system which is one of the most used operating systems in the world. Skills learned from Windows 11 can also be applied to Windows 10.

## Objectives
In this hands-on lab, you will:
- Navigate to the system drive.
- Create, rename and copy a folder.
- Create a subfolder.
- Create a file in a folder.
- Move files between folders.

## Important Notices about This Lab
### About Lab Sessions
Lab sessions are not persisted. This means that every time you connect to this lab, a new environment is created for you. Any data or files you saved in a previous session are no longer available. To avoid losing your data, plan to complete these tasks in a single session.

File and folder management is how users organize their folders and files, such as documents, pictures, and application files, on their computer.

## Exercise 1: Navigate to the System Drive
By default, the Windows operating system installs itself on the system drive. This is the C: drive by default.
1. To navigate to the C: drive, open the **File Explorer** icon on the Windows taskbar.

![](/images/Screenshot%20(442).png)

2. In the left menu scroll down and click **This PC**.

![](/images/Screenshot%20(443).png)

3. This displays the computer\'s disk drives. Double-click the **C:** drive to open it and access your files in the File Explorer.

![](/images/Screenshot%20(445).png)

You may also have other local drives on your computer, such as a D: drive, E: drive, and so on, and you may also have mapped network drives.

## Exercise 2: Create, Rename, and Copy a Folder
To create a folder, navigate to the location where you would like the folder to be created. For this example, you will use the desktop.
1. Right-click on the **Desktop** to open an options panel.

![](/images/Screenshot%20(446).png)

2. Hover over **New**, then click **Folder**.

![](/images/Screenshot%20(447).png)

A new folder will appear in your desired location. By default, the folder name will be *New folder*.

3. Click the folder name to type in a new name and press **Enter** to rename it.

![](/images/Screenshot%20(448).png)

4. If you have already navigated away from the new folder, you can right-click it to open a menu.

![](/images/Screenshot%20(449).png)

5. Click **Rename** and enter the new name. In this lab, name your folder *Renamed Folder*. When finished, press **Enter**.

![](/images/Screenshot%20(450).png)

6. To copy the folder, right-click it and click the **Copy** icon.

![](/images/Screenshot%20(451).png)

7. Then, you can navigate to the location where you want to copy the folder. In this lab, navigate to the **C:** drive. Right-click and click **Paste** to paste the copied folder.

![](/images/Screenshot%20(452).png)

## Exercise 3: Create a Subfolder
1. To create a subfolder, first navigate to the folder you want to use as a parent folder. In this example, we will use the *Renamed Folder* on our **C:** drive.

![](/images/Screenshot%20(455).png)

2. Open the folder by double-clicking it. Your folder should be empty.

![](/images/Screenshot%20(457).png)

3. Right-click in the empty window to open a menu.

![](/images/Screenshot%20(458).png)

4. Hover over **New**, then click **Folder**.

![](/images/Screenshot%20(459).png)

5. Name your folder *Subfolder*, and press **Enter**.

![](/images/Screenshot%20(460).png)

## Exercise 4: Create a File in a Folder
1. To create a new file in a folder, the process is similar to creating a new folder. First, open the location you want to create a file in. Right-click to open the options menu.

![](/images/Screenshot%20(461).png)

2. Hover over **New**, and select **Text Document** to create a new text document.

![](/images/Screenshot%20(462).png)

3. Press **Enter** to save your new text document.

![](/images/Screenshot%20(463).png)

## Exercise 5: Moving Files Between Folders
1. Next, you will learn how to move your new document. To move the *New Text Document* file from the subfolder to the parent folder, you can drag and drop your file to the new location in the address bar, or in the left pane.

![](/images/Screenshot%20(464).png)

2. This will move your text document to the folder you dragged it to, i.e. *Renamed Folder*.

![](/images/Screenshot%20(465).png)

3. To move the *New Text Document* file to the desktop, simply drag and drop the file from its location to the desktop.

![](/images/Screenshot%20(467).png)

4. This will move your file from its original location to the desktop.

![](/images/Screenshot%20(468).png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Nick Yi

### Other Contributor(s)
Steve Ryan
Andrew Pfeiffer

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-05 | 1.1 | Steve Ryan | ID review/edit |
| 2023-03-02 | 1.0 | Andrew Pfeiffer | Editing |
| 2023-02-28 | 0.1 | Nick Yi | Initial version created |
|   |   |   |   |
|   |   |   |   |
