---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4961-instruction
version-history-start-date: '2023-03-01T18:13:03Z'
---
# Sign up for Microsoft 365
**Estimated time needed:** 15 minutes

Microsoft Office 365 is a suite of office productivity software programs, collaboration tools, and cloud services. The office productivity software programs include applications that enable users to design and create documents, presentations, spreadsheets, emails, and more.

## Objectives
After completing this lab, you will be able to:
- Create a free Microsoft account


## Exercise 1 : Sign Up for a Free Microsoft Account
In this exercise, you will sign up for a Microsoft account.

1. To sign up for Microsoft 365, you need to create a free account. To do this, launch your web browser and navigate to the **Microsoft Account** page at https://account.microsoft.com.

2. On this page, click **Sign in**.

![](/images/SR_Account_1.png)

3. Then, on the **Sign in** page, click the **Create one!** link.

![](/images/SR_Account_2.png)

4. When you click the **Create one!** link, you will be directed to the **Create account** page.
5. In the text box, type in your email and click **Next**.

![](/images/O365SignUp_3.png)

6. You will then be asked to create a password, so type in the password you would like to use, and click **Next**.

![](/images/O365SignUp_4.png)

7. You will then see a screen asking you to complete a puzzle, which is required to create an account. Click **Next**.

![](/images/O365SignUp_5.png)

8. Follow the prompt to solve the puzzle to confirm you are not a robot.

![](/images/O365SignUp_6.png)

9. You will then be directed to a page where you enter your *date of birth* and *country*. Then click **Next**.

![](/images/SR_Account_7a.png)

10. Then you need to confirm your age, by clicking **Next**.

![](/images/SR_Account_8a.png)

11. Now that your account is complete, you will be directed to your Microsoft account homepage.

![](/images/SR_Account_9.png)

12. From here you can click the **App launcher** icon in the top left corner of the page.

![](/images/SR_Account_10.png)

13. From the **App launcher** menu, you can either open one of your apps, such as *Outlook*, *OneDrive*, *Word* or *PowerPoint*, or you can open the **Microsoft 365** homepage.

![](/images/SR_Account_11b.png)

14. This is the **Microsoft 365** homepage.

![](/images/SR_Account_12.png)


## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 0.3 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
| 2023-03-05 | 0.2 | Steve Ryan | Modified screenshots and new sign up process |
| 2023-03-01 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
