---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4774-instruction
version-history-start-date: '2023-02-20T18:35:07Z'
---
# Working with Junk Mail in Outlook

**Estimated time needed:** 15 minutes

For this lab, you will use Microsoft Outlook.com. Outlook.com is one of the most used online email services worldwide. Any skills learned from Outlook.com can be applied to other email applications, such as the desktop version of Microsoft Outlook, and Google Mail.

## Objectives
After completing this lab, you will be able to:
- Delete emails
- Use the Move to tool
- Move emails to the Junk folder



## Exercise 1: Deleting Emails
In this exercise, you will learn how to delete emails.

1. To delete an email, in the Inbox page on Outlook.com, hover over the email and move the cursor to the right of the email notification, and then click the **trash can** icon that appears.

![Deleting email with the Trash Can](/images/Picture1%20junk.png)

The selected email will be moved to the *Deleted Items* folder. Emails in this folder can be recovered by using the **Move to** tool.

2. To recover the deleted email, navigate to the **Deleted Items** folder in the **Folders** section.

![Recovering deleted email from Deleted Items folder](/images/Picture2%20junk.png)

3. In the folder, hover over the email notification and click the **Check mark** on the left side of the email.

![Clicking check mark on deleted email](/images/Picture3%20junk.png)

4. In the toolbar at the top of the screen, click the button labeled **Move to**. 
5. In the drop-down menu that appears, click the **Inbox** option, and the email will be moved back to the Inbox.

![Moving email from deleted folder to Inbox](/images/Picture4%20junk.png)

6. To permanently delete an email in the Deleted Items folder, hover over an email and click the **trash can** icon on the right side of the email notification.

![Permanently deleting email from folder](/images/Picture5%20junk.png)

7. In the dialog box that appears, asking you to verify that you want to delete the email permanently, click **OK**.

![Verify permanent deletion of email](/images/Picture6%20junk.png)

## Exercise 2: Moving Emails to the Junk Email Folder
In this exercise, you can learn how to move emails to the Junk Email folder.

1. To move an email to the Junk Email folder, select an email, click the **Move to** button, and in the **Search for a folder** section, start typing **Junk Email** and then select it in the list.

![Moving email to Junk folder](/images/Picture7%20junk.png)



## Author(s)
Andrew Pfeiffer

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-02-20 | 0.1 | Andrew Pfeiffer | Initial version created |
| 2023-02-22 | 0.2 | Steve Ryan | ID review |
| 2023-02-22 | 0.3 | Steve Hord | QA pass with edits |
| 2023-03-09 | 0.4 | Steve Ryan | ID 2nd review |
| 2023-03-21 | 0.5 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
|   |   |   |   |
