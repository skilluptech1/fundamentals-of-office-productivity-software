---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4934-instruction
version-history-start-date: '2023-02-27T15:15:40Z'
---
# Using Windows Apps
**Estimated time needed:** 20 minutes

For this lab, you will be using the Windows 11 operating system, which is one of the most used operating systems in the world. Skills learned from Windows 11 can also be applied to Windows 10.

## Objectives
In this hands-on lab, you will:
• Use the Calculator app
• Access and edit images in the Photos app
• Use the Task Manager app
• Search for and download apps on the Microsoft Store

## Important Notices about This Lab
### About Lab Sessions
Lab sessions are not persisted. This means that every time you connect to this lab, a new environment is created for you. Any data or files you saved in a previous session are no longer available. To avoid losing your data, plan to complete these tasks in a single session.

## Exercise 1: Using the Calculator app
In this exercise, you will use the Windows Calculator app.

1. To access the Calculator app, navigate to the **Search bar** at the bottom of the screen, on the **Taskbar** click **Search**.

![](/images/Screenshot%20(469).png)

2. A panel will open, click the app labeled **Calculator**.

![](/images/Screenshot%20(470).png)

3. The Calculator app will open.

![](/images/Screenshot%20(471).png)

4. To view the different modes and features of the Calculator app, click the **Menu** button on the top left of the panel.

![](/images/Screenshot%20(472).png)

## Exercise 2: Accessing and editing images in the Photos app
In this exercise, you will access and edit images in the Photos app.

1. To access the Photos app on your computer, first navigate to the **Search bar** on the **Windows Taskbar**

![](/images/Screenshot%20(484).png)

2. In the **Search bar** type **Photos** and click on the app icon.

![](/images/Screenshot%20(485).png)

3. To edit a image, double-click a picture.

![](/images/Screenshot%20(486).png)

4. Click the **Edit** button on the **Toolbar** at the top of the screen.

![](/images/Screenshot%20(487).png)

5. Click the **Crop** icon to start cutting parts of the image out to resize it.

![](/images/Screenshot%20(559).png)

6. Use your mouse to adjust the image by clicking and dragging the white corner handles, and the white lines on each of the sides of the image. Then click **Save as copy**. Another panel will open, click **Save**.

![](/images/Screenshot%20(560)111.png)

The image will be saved to your **Pictures** folder.

## Exercise 3: Use the Task Manager app
In this exercise, you will use the Task Manager app.

1. To access the Task Manager app, navigate to the **Search bar** on the **Windows Task bar** and type **Task Manager** in the **Search bar**.

![](/images/Screenshot%20(473).png)

2. Click on the **Task Manager** app to open. This will display information about the running apps and processes on your computer, including how much CPU, memory, and disk is being used by each app and process.

![](/images/Screenshot%20(474).png)

3. To access the other features of the app, use the left menu to view performance, app history, and more.

![](/images/Screenshot%20(475).png)

4. Another way to access the Task Manager app is to simultaneously press the **Ctrl** + **Alt** + **Delete** keys. The screen will go black, and the **Menu** seen below will appear. From this menu click **Task Manager**.

![](/images/1111111.png)

## Exercise 4: Searching for and downloading apps on the Microsoft Store
In this exercise, you will search for and download apps on the Microsoft Store.

1. To access and download apps from the Microsoft Store, first navigate to the **Windows Taskbar** and click on the **Microsoft Store** app icon.

![](/images/Screenshot%20(476).png)

2. The Microsoft Store homepage will open.

![](/images/Screenshot%20(478).png)

3. On the Microsoft Store, you can download apps, movies, games, and more. You can search for these items by using the **Search bar** at the top of the screen, or the **Menu panel** on the left side of the screen.

![](/images/Screenshot%20(490).png)

4. In the **Search bar** type **Adobe Acrobat Reader DC**, to download it click the **Install** button. The app will be avialable in the **Start menu**.

![](/images/Screenshot%20(480).png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer, Stephanie Johnsen

### Other Contributor(s)
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-09 | 1.1 | Steve Ryan | 2nd ID review\edit |
| 2023-03-07 | 1.0 | Andrew Pfeiffer | Review\edit |
| 2023-03-05 | 0.2 | Steve Ryan | ID review\edit |
| 2023-03-02 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
