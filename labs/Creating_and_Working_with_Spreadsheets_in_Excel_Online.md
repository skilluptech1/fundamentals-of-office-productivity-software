---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4812-instruction
version-history-start-date: '2023-02-22T16:28:08Z'
---
# Creating and Working with Spreadsheets in Excel Online
**Estimated time needed:** 30 minutes

For the demonstrations, you will use Microsoft\'s \'Excel for the web\' which is one of the most used online spreadsheet applications in the world. Skills learned from \'Excel for the web\' can be applied to other spreadsheet apps such as the desktop version of Microsoft Excel, and Google Sheets.

## Objectives
After completing this lab, you will be able to:
- Create a chart
- Share a worksheet to work collaboratively online
- Upload spreadsheets


## Exercise 1: Create a chart
In this exercise, you will learn how to create a Pie Chart in \'Excel for the web\'.

1. In an open blank workbook, type multiples of 20 into the cells from **A1** to **E1** (i.e. **20**, **40**, **60**, **80**, **100**).
![](/images/Screenshot%20(561).png)

2. Next, in the toolbar at the top of the screen, on the **Insert** tab, click the **Pie Chart** icon.
![](/images/Screenshot%20(562).png)

3. A pie chart will be inserted into the workbook using the data you entered.
![](/images/Screenshot%20(563).png)

4. To edit the chart\'s title, double click on the title text labeled **Chart Title**. A panel will open on the right side of the screen.
![](/images/Screenshot%20(564).png)

5. In the **Format** tab, click on the drop-down arrow in the **Chart Title** section, to open the menu.
![](/images/Screenshot%20(565).png)

6. In the **Chart Title** text box, type **Sales Numbers**. Under the **Font** section, click the **Bold** icon.
![](/images/Screenshot%20(567).png)

7. Next, open the **Legend** drop-down menu.
![](/images/Screenshot%20(568).png)

8. Change the legend position from **Bottom** to **Top**.
![](/images/Screenshot%20(569).png)

9. Finally, close the panel by clicking the **X** button in the top right corner.
![](/images/Screenshot%20(572).png)

Any type of chart or graph can be created, edited, and formatted following these steps.

## Exercise 2: Share a workbook to work collaboratively online
In this exercise, you will learn how to share a workbook so that you can work collaboratively online in \'Excel for the web\'.

1. In the open workbook, navigate to the top right corner of the screen and click **Share**.
![](/images/Screenshot%20(573).png)

2. In the drop-down menu, click the **Share** option.
![](/images/Screenshot%20(574).png)

3. In the panel that opens, in the email address text box type **Email@outlook.com**. Click the suggested email address that appears.
![](/images/Screenshot%20(575).png)

4. To add another email address, type **Outlook@Outlook.com**, and click the suggested email address that appears. Then click **Send**.
![](/images/Screenshot%20(576).png)

## Exercise 3: Upload a workbook
In this exercise, you will learn how to upload a workbook in \'Excel for the web\'.

1. First, download the workbook you created by clicking **File** in the menu bar at the top of the screen.
![](/images/Screenshot%20(577).png)

2. Click **Save As**, then click **Download a Copy**.
![](/images/Screenshot%20(578).png)

3. You will get a download notification in the top right corner of the screen.
![](/images/Screenshot%20(579).png)

4. Next, navigate back to the Excel homepage by clicking the **App launcher** button in the top left corner of the screen.
![](/images/Screenshot%20(580).png)

5. In the App launcher, click the **Excel** icon.
![](/images/Screenshot%20(581).png)

6. You will then be directed to the Excel homepage. On the homepage click the **Upload** button on the right side of the screen.
![](/images/Screenshot%20(583).png)

7. In the file explorer panel, click the **Downloads** folder, select the workbook you downloaded, and click **Open**.
![](/images/Screenshot%20(584).png)

8. You will now be able to find your uploaded workbook on the homescreen.
![](/images/Screenshot%20(585).png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s)
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 1.2 | Steve Ryan | Removed \'About Lab Sessions\' text |
| 2023-03-09 | 1.1 | Steve Ryan | ID review |
| 2023-03-08 | 1.0 | Andrew Pfeiffer | Updates made |
| 2023-02-22 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
