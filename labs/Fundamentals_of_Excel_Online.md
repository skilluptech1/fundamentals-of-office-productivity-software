---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4783-instruction
version-history-start-date: '2023-02-21T16:51:10Z'
---
# Fundamentals of Excel Online
**Estimated time needed:** 40 minutes

For the demonstrations, Microsoft\'s \'Excel for the web\' will be used, which is one of the most used online spreadsheet applications in the world. Skills learned from \'Excel for the web\' can be applied to other spreadsheet apps such as the desktop version of Microsoft Excel, and Google Sheets.

## Objectives
After completing this lab, you will be able to:
- Create spreadsheets
- Share spreadsheets
- Use basic formulas in Excel

## Exercise 1: Create a Spreadsheet
In this exercise, you will create a spreadsheet.

1. To create a spreadsheet navigate to **Office.com** and login to your account. On the Office homepage, open **Excel** by clicking the icon on the panel located on the left side of the screen.

![](/images/Screenshot%20(539).png)

2. You will be directed to the Excel home screen. To create a new spreadsheet, click **New blank workbook** under the Create New tab.

![](/images/Screenshot%20(540).png)

3. Rename the spreadsheet by clicking on the file name in the top left corner of the screen. Rename the spreadsheet to **Test Spreadsheet** and press **Enter**.

![](/images/Screenshot%20(544).png)

4. To download the spreadsheet, in the toolbar, click **File**.

![](/images/Screenshot%20(543).png)

5. In the panel, click **Save As**, and then click **Download a Copy**.

![](/images/Screenshot%20(545).png)

6. To upload a document, navigate to the Excel homepage, and click the **Upload** button.

![](/images/Screenshot%20(546).png)

7. In the file explorer wndow, click the **Downloads** folder, select the **Test Spreadsheet**, and then click **Open**.

![](/images/Screenshot%20(547).png)

The spreadsheet will open in an Excel workbook.

## Exercise 2: Share a Spreadsheet
In this exercise, you will share a spreadsheet by email.

1. Open the **Test Spreadsheet** file, and click the **Share** drop-down menu in the top right corner.

![](/images/Screenshot%20(548).png)

2. In the menu, click **Share**.

![](/images/Screenshot%20(550).png)

3. In the dialog box that appears, in the email address bar type **Testemail@outlook.com** and click **Send**.

![](/images/Screenshot%20(552).png)

## Exercise 3: Using Formulas in Excel
In this exercise, you will learn how to use formulas in Excel.

1. In the cells **A1**, **B1**, **C1**, and **D1**, type in ascending order multiples of five (i.e. **5**, **10**, **15**, **20**).

![](/images/Screenshot%20(554).png)

2. For this exercise you will do an addition equation. In cell **E1** type the **equals** symbol and type **Sum** which is the term for addition. In the small menu that appears, click the **SUM** option.

![](/images/Screenshot%20(555).png)

3. Next after the open parenthesis type **A1,B1,C1,D1** then type a close parenthesis by typing the **)** character. When finished typing press **Enter** to enter the equation and see the result.

![](/images/Screenshot%20(557).png)

Different formulas can be used to subtract, add, multiply, divide, and more.

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s)
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-09 | 1.1 | Steve Ryan | ID review |
| 2023-03-07 | 1.0 | Andrew Pfeiffer | Revisions made |
| 2023-02-21 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
