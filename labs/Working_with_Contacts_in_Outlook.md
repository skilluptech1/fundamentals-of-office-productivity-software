---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4781-instruction
version-history-start-date: '2023-02-21T14:07:35Z'
---
# Working with Contacts in Outlook
**Estimated time needed:** 20 minutes

For this lab, you will use Microsoft Outlook.com. Outlook.com is one of the most used online email services worldwide. Any skills learned from Outlook.com can be applied to other email applications such as the desktop version of Microsoft Outlook, and Google Mail.

## Objectives
After completing this lab, you will be able to:
- Add contacts
- Add contacts to favorites
- Delete contacts


## Exercise 1: Add a Contact
In this exercise, you will add a contact.

1. To add a contact, find and open an email from the person you would like to add as a contact.

2. There will be a contact icon at the left corner of the message with initials. Click the **initials icon** and a panel will appear.

![Click initials icon and panel appears](/images/contact1.png)

3. Click the **Contact** tab and then click **Add to contacts**.

![Add to contacts](/images/contact2.png)

4. Another panel will open with a form to complete the contact information. Add the contact information, and click **Save**.

![Fill in form with contact information](/images/contact3.png)

## Exercise 2: Add a Contact to Favorites
In this exercise, you will add a contact to favorites.

1. To add a contact to favorites, navigate to the left navigation menu, and click the **People** icon.

![](/images/Screenshot%20(492).png)

2. On the **Contacts** page, click the contact you would like to add to favorites, click the **More options** (...) button on the contact\'s page, and click **Add to favorites**.

![Add contact to favorites](/images/contact5.png)

3. To remove a contact from favorites, open a contact from the favorites list and click the **Remove from favorites** button on the toolbar.

![Remove from favorites](/images/contact6.png)

## Exercise 3: Delete a Contact
In this exercise, you will delete a contact.

1. To delete a contact, open the person\'s contact information, and click the **Delete** button on the toolbar.

![Highlight contact and click Delete button](/images/contact7.png)

2. To permanently delete a contact go to the **Deleted** folder, select the deleted contact, and click the **Delete** button on the toolbar.

![Permanently remove contact from Deleted folder](/images/contact8.png)

3. To recover the deleted contact go to the **Deleted** folder, select the deleted contact, and click the **Restore** button.

![Restore contact from Deleted folder](/images/contact9.png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-02-21 | 0.1 | Andrew Pfeiffer | Initial version created |
| 2023-02-22 | 0.2 | Steve Ryan | ID review |
| 2023-02-22 | 0.3 | Steve Hord | QA pass with edits |
| 2023-02-23 | 0.4 | Steve Ryan  | Minor post-QA edits  |
| 2023-03-21 | 0.5 | Steve Ryan  | Removed \'About Lab Sessions\' paragraph  |
