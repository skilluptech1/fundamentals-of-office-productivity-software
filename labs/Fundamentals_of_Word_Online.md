---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4938-instruction
version-history-start-date: '2023-02-27T17:51:21Z'
---
# Fundamentals of Word Online
**Estimated time needed:** 20 minutes

For this lab, you will use Microsoft\'s \'Word for the web\', which is one of the most used online word-processing software applications worldwide. Any skills learned from \'Word for the web\' can be applied to other word-processing applications such as the desktop version of Microsoft Word, and Google Docs.

## Objectives
After completing this lab, you will be able to:
- Create documents
- Share files
- Insert images
- Download files



## Exercise 1 : Create a document
In this exercise, you will learn how to create documents in \'Word for the web\'.

1. To create a document, sign in at **Office.com**, and click on the **Word** icon on the left side of the screen.

![](/images/Screenshot%20(493).png)

2. In the top panel, click **New blank document**.

![](/images/Screenshot%20(494).png)

3. A blank document will open.

![](/images/Screenshot%20(495).png)

## Exercise 2 : Share a document
In this exercise, you will share a Word document through email.

1. To share the Word document, naviagte to the top right corner of the screen and click **Share**.

![](/images/Screenshot%20(496).png)

2. In the drop-down menu, click **Share**.

![](/images/Screenshot%20(497).png)

3. A dialog box will open with fields to complete.

![](/images/Screenshot%20(498).png)

4. In the text box labeled **To: Name, group or email**, enter the following email address: **Testemail@outlook.com**.

![](/images/Screenshot%20(499).png)

5. Next, to change the user permission so they will only be able to view the document, click **Anyone with the link can edit**.

![](/images/Screenshot%20(500).png)

6. In the **Sharing settings** dialog box, under **More settings**, click the drop-down arrow on the **Can edit** tab.

![](/images/Screenshot%20(501).png)

7. Click **Can view**.

![](/images/Screenshot%20(502).png)

8. Then save the changes by clicking **Apply**.

![](/images/Screenshot%20(504).png)

9. You will be directed back to the **Send link** dialog box. Click **Send**.

![](/images/Screenshot%20(506).png)

People with a link to the document will only be able to view the file and cannot make edits.

## Exercise 3 : Insert images
In this exercise, you will learn how to insert images in a Word document.

1. First, in an open document, click the **Insert** tab on the toolbar and click **Picture**.

![](/images/Screenshot%20(507).png)

2. In the **Picture** drop-down menu click **Stock Images**.

![](/images/Screenshot%20(508).png)

3. In the search bar type **Computer**.

![](/images/Screenshot%20(509).png)

4. Choose an image containing a computer, and click **Insert** in the bottom right of the window.

![](/images/Screenshot%20(510).png)

The image will be inserted into the document.

![](/images/Screenshot%20(512).png)

6. To adjust the size of the image, click on it, and note the circle icons on each corner and side of the image.

![](/images/Screenshot%20(513).png)

7. Hold the **Shift** key and with a **left mouse click**, drag the **Circle** on the top right corner towards the left of the document until the image gets smaller and fills approximately half of the page, then release.

![](/images/Screenshot%20(514).png)

The image can be resized to be larger or smaller at anytime.

## Exercise 4 : Downloading Word documents
In this exercise, you will learn how to download Word documents.

1. To download a document, click **File** in the toolbar.

![](/images/Screenshot%20(515).png)

2. In the left menu, click **Save as**.

![](/images/Screenshot%20(516).png)

3. In the **Save as** dialog box click **Download a Copy**.

![](/images/Screenshot%20(517).png)

4. In the notification panel that appears, click **Download a copy**.

![](/images/Screenshot%20(519).png)

5. This will open a new browser tab, and in the top right of the screen there will be a download notification. Click on it, and click the **Open file** link.

![](/images/Screenshot%20(520).png)

Downloaded documents can also be found in the **Downloads** section of the **Files** folder.

6. The document will open in your desktop version of Microsoft Word if you have it installed. In the tooltip that appears at the top of the screen, click **Enable Editing** to edit the document.

![](/images/Screenshot%20(521).png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s)
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 1.2 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
| 2023-03-08 | 1.1 | Steve Ryan | ID Review |
| 2023-03-07 | 1.0 | Andrew Pfeiffer | Revisions made |
| 2023-02-27 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
