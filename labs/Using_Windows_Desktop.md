---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4899-instruction
version-history-start-date: '2023-02-24T13:59:20Z'
---
# Using Windows Desktop
**Estimated time needed:** 20 minutes

For this lab, you will be using the Windows 11 operating system which is one of the most used operating systems in the world. Skills learned from Windows 11 can also be applied to Windows 10. 

## Objectives
After completing this lab, you will be able to:
- Access Settings
- Pin apps to the Start menu
- Add shortcuts to the desktop
- Use the Recycle Bin


## Exercise 1: Accessing Settings
In this exercise, you will learn how to access Settings.

1. To access **Settings** in Windows 11, navigate to the taskbar and click the **Start** button.

2. In the **Start menu**, click the **Settings** icon.

![Start menu highlights Settings button](/images/Picture1Window.png)

After clicking the Settings icon, the Settings page will open. On the Settings page users can update their PC, access network settings, change wallpapers, configure controls, and more.

![System settings window](/images/Picture2Window.png)


## Exercise 2: Pinning Apps to the Start Menu

1. To pin an app to the **Start menu** first find an app either on the taskbar or desktop and right-click the icon. In the menu, select the **Pin to Start** option.

![App menu highlights Pin to Start option](/images/Picture3Window.png)

Applications that are pinned on the Start menu can be unpinned.

2. In the Start menu, right-click the application\'s icon, and click **Unpin from Start**.

![App menu highlights Unpin from Start option](/images/Picture4Window.png)

## Exercise 3: Adding Shortcuts to the Desktop
In this exercise, you will learn how to add shortcuts to the desktop.

1. To add a shortcut to the desktop, navigate to an application either on the Start menu or in a folder in File Explorer.

2. Drag the application to the desktop using the left mouse button and release the mouse button.

![Dragging app to desktop](/images/Picture5Window.png)

3. Shortcuts can be removed from the desktop by right-clicking the icon and selecting the **trash can** icon.

![Shortcut menu highlights trash can icon](/images/Picture6Window.png)

## Exercise 4: Using the Recycle Bin
In this exercise, you will learn how to use the features of the Recycle Bin.

1. To recover an item from the **Recycle Bin** right-click the item you want to recover and click the **Restore** button.

2. To permanently delete the item, right-click the item and click the **trash can** icon.

![Recycle Bin item delete with trash can icon](/images/Picture7Window.png)

## Congratulations! You have completed this lab and are ready for the next topic.


## Author(s)
Andrew Pfeiffer

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-02-24 | 0.3 | Steve Hord | QA pass with edits |
| 2023-02-24 | 0.2 | Steve Ryan | ID review/edits |
| 2023-02-24 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
