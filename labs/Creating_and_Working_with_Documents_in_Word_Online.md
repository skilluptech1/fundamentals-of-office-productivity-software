---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4948-instruction
version-history-start-date: '2023-02-28T17:35:00Z'
---
# Creating and Working with Documents in Word Online
**Estimated time needed:** 20 minutes
In this lab, you will use Microsoft\'s \'Word for the web\', which is one of the most used online word-proccessing software applications worldwide. Any skills learned from \'Word for the web\' can be applied to other word-processing applications such as the desktop version of Microsoft Word, and Google Docs.

## Objectives
After completing this lab, you will be able to:
- Use templates
- Add comments
- Use the Protect Document tool


## Exercise 1: Using templates
In this exercise, you will create a document from a template in \'Word for the web\'.

1. To create a document from a template, navigate to **Office.com** and click on the **Word** icon on the left panel.

![](/images/Screenshot%20(523).png)

2. You will be directed to the Word homescreen.

![](/images/Screenshot%20(524).png)

3. To view all templates, click **More templates** on the right side of the page.

![](/images/Screenshot%20(525).png)

4. You will be directed to the template library.

![](/images/Screenshot%20(526).png)

5. Click on the **MLA style paper** template.

![](/images/Screenshot%20(527).png)

6. A document using the selected template will open.

![](/images/Screenshot%20(529).png)

## Exercise 2: Adding comments
In this exercise, you will add comments to a document in \'Word for the web\'.

1. To add a comment to a document, select some text and on the **Insert** tab, click **New comment**.

![](/images/Screenshot%20(531).png)

2. A draft comment will be generated. In the comment text box, type **New template created**, and click the **Send** icon.

![](/images/Screenshot%20(532).png)

3. To mark the comment as **Resolved**, click the ellipsis **(...)** button in the comment and click **Resolve thread**.

![](/images/Screenshot%20(533).png)

Marking the comment as resolved means the issue has been fixed. This function is especially helpful in group projects.

## Exercise 3: Using the Protect Document tool
In this exercise, you will use the Protect Document tool. This tool protects a document from accidental changes by others, so the document will only allow edits by you.

1. To use the Protect Document tool, in the toolbar, click **File**.

![](/images/Screenshot%20(534).png)

2. In the panel, click **Info**.

![](/images/Screenshot%20(535).png)

3. In the Info panel, click **Protect Documennt**, and the document will now be protected and marked as view-only.

![](/images/Screenshot%20(537).png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

### Other Contributor(s)
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 1.2 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
| 2023-03-09 | 1.1 | Steve Ryan | ID review |
| 2023-03-07 | 1.0 | Andrew Pfeiffer | Revisions made |
| 2023-02-28 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
