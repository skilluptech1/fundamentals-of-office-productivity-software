---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4750-instruction
version-history-start-date: '2023-02-17T19:38:40Z'
---
# Email Fundamentals
**Estimated time needed:** 20 minutes

For this lab, you will use Microsoft Outlook.com. Outlook.com is one of the most popular email service providers worldwide. Any skills learned from Outlook.com can be applied to other email applications such as Outlook for desktop and Google Mail.

## Objectives
After completing this lab, you will be able to:
- Create emails
- Insert attachments
- Send emails
- Search and sort inboxes


## Exercise 1 : Create an Email
In this exercise, you will create an email and add a subject line.

1. To create an email, navigate to the Inbox page. On the top left side of the screen, click **New mail**.

![Outlook Home menu with New mail highlighted](/images/Picture1.png)


A **draft** email will appear in a panel on the screen.


![Draft email](/images/Picture2.png)

2. The first step in creating an email is to type the address of your intended recipient. Enter the email address of a friend or colleague in the textbox labeled **To**.

![Draft email with To line highlighted](/images/Picture3.png)

3. The next step is to fill in the **Subject** line. In the **Subject** line, type *Sales Proposal* and press Enter.

![Draft email with Add a subject highlighted](/images/Picture4.png)

The large text box under the Subject line is the body text box.

![Draft email with body text box highlighted](/images/Picture5.png)

## Exercise 2 : Insert an Image and an Attachment
In this exercise, you will insert an image and a file.

1. To insert an image, click the **Picture** icon at the end of the draft email window. Clicking the icon will open your computer\'s **Pictures** folder.

![Draft email with Picture icon highlighted](/images/Picture6.png)

2. Select any image file on your computer to insert into the email. (To add multiple pictures, hold the Ctrl key and click each of the images.)

3. After selecting the image, click **Open** in the bottom right of the **Pictures** window.

![Pictures panel with Open button highlighted](/images/Picture7.png)

4. To insert a document as an attachment, click the **Paperclip** icon in the bottom left of the draft email window.

![Draft email with Paperclip icon highlighted](/images/Picture8.png)

5. After clicking the **Paperclip** icon, a small menu will appear with different locations to select a file. The three options are: *Browse this computer*, *OneDrive*, and *Upload and share*.

6. We will choose a file from your computer. So, on the menu, click **Browse this computer**, and a window will open.

7. Select any file on your computer, and click **Open**.

![Select the file screen with Open button highlighted](/images/Picture9.png)

## Exercise 3 : Send an Email

In this exercise, you will send an email.

1. The final step of creating an email is to send it. There is a **Paper airplane** button on the lower left of the draft email window. When ready, click the **Paper airplane** to send the email.

![Email shows the Paper airplane button highlighted](/images/Picture10.png)

## Exercise 4: Search and Sort the Inbox

In this exercise, you will use the **Search** and **Sort** features in the Inbox.

1. Knowing how to navigate your Inbox is essential. To search your Inbox, navigate to the Inbox page of Outlook.com. You click the **Search bar** to search for contacts or emails.

![Outlook.com Inbox shows Search bar highlighted](/images/Picture11.png)

2. In the search bar, enter *Proposal* and press Enter. Outlook.com will provide search results based on the people you have emailed or received messages from.
![Inbox shows ops in Search bar with results for emails named ops](/images/Picture12.png)

3. To sort your Inbox, navigate to the Inbox page and click the **Filter** at the top right of the **Focused** panel.
![Inbox page with Filter icon highlighted](/images/Picture13.png)

4. A drop-down menu will open. Click **Sort**.
![Filter drop-down menu shows Sort is highlighted](/images/Picture14.png)

5. After clicking **Sort**, a menu panel will appear. The **Sort by** options are *Date*, *From*, *Size*, *Importance*, and *Subject*.

![Sort by menu explained in Step 5](/images/Picture15.png)

6. In the menu, select **Size**.

The **Sort order** has two possibilities: **Oldest on top** and **Newest on top**.

7. Choose **Oldest on top**.

## Congratulations! You have completed this lab and are ready for the next topic.

<!--## (Optional) Summary / Conclusion / Next Steps
Add ending info here, if required, and rename the title accordingly. Otherwise, remove this optional section.-->

## Author(s)
Andrew Pfeiffer
Steve Ryan

### Other Contributor(s) 
Steve Hord 

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 1.4 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
| 2023-02-22 | 1.3 | Steve Ryan | Minor edit to opening paragraph |
| 2023-02-20 | 1.2 | Steve Ryan | Minor edits |
| 2023-02-17 | 1.1 | Steve Hord | QA pass with edits |
| 2023-02-17 | 1.0 | Andrew Pfeiffer | Lab completion |
| 2023-02-17 | 0.2 | Andrew Pfeiffer | Minor edits |
| 2023-02-17 | 0.1 | Steve Ryan | Initial lab draft |




