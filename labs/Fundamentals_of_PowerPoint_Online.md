---
markdown-version: v1
tool-type: instructional-lab
branch: lab-4817-instruction
version-history-start-date: '2023-02-22T18:38:19Z'
---
# Fundamentals of PowerPoint Online
**Estimated time needed:** 30 minutes

For this lab, we will use \'PowerPoint for the web\', which is one of the most popular and widely used online presentation applications available. Any skills learned from \'PowerPoint for the web\' can be applied to other presentation applications such as the desktop version of Microsoft PowerPoint, and Google Sheets.


## Objectives
After completing this lab, you will be able to:
- Create, edit, and save a presentation
- Format text
- Preview a presentation
- Present slide shows



## Exercise 1: Create, edit, and save a presentation
In this exercise, you will create a new presentation.

1. To create a presentation, sign in to Microsoft 365 and click the **PowerPoint** icon in the left menu. Under **Create new**, click **Blank presentation**.
![](/images/Picture1p.png)

2. This will open a new blank presentation.
![](/images/Picture2p.png)

3. To edit a presentation, click the location where you want to add text and type *This is text that I want to delete*.
![](/images/Picture3p.png)

4. To delete text, click the location where you want to remove text and drag your cursor over the text.
![](/images/Picture4p.png)

5. Press the **Delete** or **Backspace** key on your keyboard to delete the text.
![](/images/Picture5p.png)

6. \'PowerPoint for the web\' automaticallys save as you make changes to your presentation. However, you may occasionally want to manually save a copy of a presentation. To do so, click **File**, then **Save as**.
![](/images/Picture6p.png)

7. Click **Save as** again.
![](/images/Picture7p.png)

8. Give your file a name, then click **Save**. This will save a copy to your OneDrive location.
![](/images/Picture8p.png)

## Exercise 2: Format text in a presentation
In this exercise, you will format text in your presentation.

1. Edit the slide by adding the text, *This is text I want to format*.
![](/images/Picture9p.png)

2. To format your text, first select the text you want to format.
![](/images/Picture10p.png)

3. Make you font bold by selecting the **Bold** option from the toolbar.
![](/images/Picture11p.png)
![](/images/Picture12p.png)

4. Add a color highlight to your text by selecting the **Highlight** button.
![](/images/Picture13p.png)
![](/images/Picture14p.png)

5. Change the font to **Consolas** by selecting it from the **Font** drop-down menu.
![](/images/Picture15p.png)
![](/images/Picture16p.png)

6. Change the color of your text by clicking the drop-down menu for font colors.
![](/images/Picture17p.png)

7. Change the text to red.
![](/images/Picture18p.png)

## Exercise 3: Preview a presentation
In this exercise, you will preview your presentation.

1. Click on the **View** tab. You will see two options to view your presentation: **Slide Sorter** and **Normal**.

2. **Normal** view is the default. In this view you see your selected slide in the main window and on the left hand side, you will see a thumbnail list of all the slides in your presentation.
![](/images/Picture19p.png)
![](/images/Picture20p.png)

3. In **Slide Sorter** view, you will see a grid of thumbnails of all your slides.
![](/images/Picture21p.png)

## Exercise 4: Present a slide show
In this exercise, you will present a slide show.

1. When you\'re satisfied with your slides, you\'re ready to present. To present your slides, click the **Slide Show** tab.
![](/images/Picture22p.png)

2. Next, select **From Beginning** to begin presenting your slide show from the first slide.
![](/images/Picture23p.png)

3. In the slide show, you can navigate between slides by left-clicking with your mouse, or by using the arrow keys.


## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer, Nick Yi

### Other Contributor(s) 
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 03/21/23 | 0.5 | Steve Ryan  | Removed \'About Lab Sessions\' paragraph   |
| 03/09/23 | 0.4 | Steve Ryan  | ID review   |
| 03/08/23 | 0.3 | Andrew Pfeiffer    | Added missing lab steps   |
| 02/23/23 | 0.2 | Nick Yi    | Built lab steps   |
| 02/22/23 | 0.1 | Steve Ryan | Initial draft created |
|   |   |   |   |
|   |   |   |   |
