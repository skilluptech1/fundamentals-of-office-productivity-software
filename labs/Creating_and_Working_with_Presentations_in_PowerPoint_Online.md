---
markdown-version: v1
tool-type: instructional-lab
branch: lab-5042-instruction
version-history-start-date: '2023-03-08T21:07:08Z'
---
# Creating and Working with Presentations in PowerPoint Online
**Estimated time needed:** 20 minutes

For this lab, you will use \'PowerPoint for the web\', which is one of the most popular and widely-used online presentation applications available. Any skills learned from \'PowerPoint for the web\' can be applied to other presentation applications such as the desktop version of Microsoft PowerPoint, and Google Sheets.

## Objectives
After completing this lab, you will be able to:
- Use a template
- Insert images
- Add animations
- Work collaboratively on a presentation


## Exercise 1: Use a template
In this exercise, you will create a new presentation by using a PowerPoint template.

1. To use a template, click **File** then **New**.
![](/images/Picture1ppt.png)

2. You will see several pre-made templates you can choose to base your slide show on. Click on your desired template to use it. For this lab, select the **Geometric color block** template.
![](/images/Picture2ppt.png)

3. This will create a new presentation from the selected template.
![](/images/Picture3ppt.png)


## Exercise 2: Insert images
In this exercise, you will insert images into your presentation.

1. There are several ways to insert an image into a presentation. To insert an image from your local device click the **Insert** tab.
![](/images/Picture4ppt.png)

2. In the toolbar, click **Pictures** to open a drop-down menu. From this menu, you can choose where you want to insert pictures from. Click **This Device**.
![](/images/Picture5ppt.png)

3. This will open your file explorer. Navigate to the image you want to add to the presentation, select it, then click **Open**. This will insert the image into your slide.
![](/images/Picture6ppt.png)
![](/images/Picture7ppt.png)

## Exercise 3: Add animations
In this exercise, you will add animations to your presentation.

1. To add an animation to your slide show, click the object you want to add an animation to. In this lab, click the **Image** you just inserted. You should see a box around the selected object.
![](/images/Picture8ppt.png)

2. Next, click the **Animations** tab.
![](/images/Picture9ppt.png)

3. Click the **Arrow** to open a drop-down menu of animations.
![](/images/Picture10ppt.png)

4. Click **Fade** to add the fade animation to your image. This will animate your object so it fades in during the slideshow.
![](/images/Picture11ppt.png)

## Exercise 4: Work collaboratively on a presentation
In this exercise, you will see how to work collaboratively on your presentation.

1. While working collaboratively in real-time in \'PowerPoint for the web\', you can see who is currently working on a file by looking in the top right. An icon will appear with your collaborator\'s initials or profile picture.
![](/images/Picture12ppt.png)

2. A key aspect of working collaboratively is being able to work with comments. To add a comment, first go to the slide you’d like to leave a comment on. Click **Review** then **New Comment**.
![](/images/Picture13ppt.png)

3. A box will appear where you can leave a comment. Type **This is a comment** into the text field, then press the **red post** button to leave the comment.
![](/images/Picture14ppt.png)

4. You can also reply to a comment by typing in the **Reply** box at the bottom of a comment. Type **This is a reply** in the reply field. When you press the **red post** button, you will add an additional comment in response to the original poster, which starts to create a thread of comments.
![](/images/Picture15ppt.png)

## Congratulations! You have completed this lab and are ready for the next topic.

## Author(s)
Andrew Pfeiffer

## Other Contributors
Steve Ryan

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2023-03-21 | 0.3 | Steve Ryan | Removed \'About Lab Sessions\' paragraph |
| 2023-03-09 | 0.2 | Steve Ryan | ID review |
| 2023-03-08 | 0.1 | Andrew Pfeiffer | Initial version created |
|   |   |   |   |
|   |   |   |   |
